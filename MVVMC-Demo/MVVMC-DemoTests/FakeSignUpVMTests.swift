//
//  FakeSignUpVMTests.swift
//  MVVMC-DemoTests
//
//  Created by Mason Chang on 2024/4/30.
//

import XCTest
import MVVMC_Demo

final class FakeSignUpVMTests: XCTestCase {

    var sut: FakeSignUpVM!
    var mockRegexChecker: MockRegexChecker!
    
    override func setUp() {
        super.setUp()
        mockRegexChecker = MockRegexChecker()
        sut = FakeSignUpVM(regexChecker: mockRegexChecker)
    }
    
    override func tearDown() {
        mockRegexChecker = nil
        sut = nil
        super.tearDown()
    }
    
    func testSignUpWithCorrentNameButIncorrectPassword() {
        // Given:
        let name = "pass"
        let password = "not"
        let expectation = XCTestExpectation(description: "Signup")
        
        // When:
        var error: Error?
        sut.signup(name: name, password: password) { err in
            error = err
            expectation.fulfill()
        }
        
        // Then:
        wait(for: [expectation], timeout: 0.1)
        XCTAssertNotNil(error)
        XCTAssertEqual(mockRegexChecker.methodCalled, [.checkName(name), .checkPassword(password)])
    }
    
    func testSignUpWithCorrentNameAndPassword() {
        // Given:
        let name = "pass"
        let password = "pass"
        let expectation = XCTestExpectation(description: "Signup")
        
        // When:
        var error: Error?
        sut.signup(name: name, password: password) { err in
            error = err
            expectation.fulfill()
        }
        
        // Then:
        wait(for: [expectation], timeout: 0.1)
        XCTAssertNil(error)
        XCTAssertEqual(mockRegexChecker.methodCalled, [.checkName(name), .checkPassword(password)])
    }
    
    func testSignUpWithIncorrectName() {
        // Given:
        let name = "not"
        let password = "not"
        let expectation = XCTestExpectation(description: "Signup")
        
        // When:
        var error: Error?
        sut.signup(name: name, password: password) { err in
            error = err
            expectation.fulfill()
        }
        
        // Then:
        wait(for: [expectation], timeout: 0.1)
        XCTAssertNotNil(error)
        XCTAssertEqual(mockRegexChecker.methodCalled, [.checkName(name)])
    }
    
    class MockRegexChecker: RegexChecker {
        
        enum Method: Equatable {
            case checkName(String)
            case checkPassword(String)
        }
        
        var methodCalled: [Method] = []
        
        func checkNameFormat(_ name: String) -> Bool {
            methodCalled.append(.checkName(name))
            if name.contains("pass") {
                return true
            } else {
                return false
            }
        }
        
        func checkPasswordFormat(_ password: String) -> Bool {
            methodCalled.append(.checkPassword(password))
            if password.contains("pass") {
                return true
            } else {
                return false
            }
        }
    }
}
