//
//  AppCoordinator.swift
//  MVVMC-Demo
//
//  Created by Mason Chang on 2024/4/29.
//

import Foundation
import UIKit

class AppCoordinator: BaseCoordinator {
    
    var window = UIWindow(frame: UIScreen.main.bounds)
    
    override func start() {
        window.makeKeyAndVisible()
        window.rootViewController = navController
        
        let signUpCoordinator = SignInSignUpCoordinator(signInViewModel: FakeSignInVM(),
                                                        signUpViewModel: FakeSignUpVM())
        signUpCoordinator.navController.modalPresentationStyle = .fullScreen
        navController.present(signUpCoordinator.navController, animated: false)
        start(child: signUpCoordinator)
    }
    
    override func didFinish(_ coordinator: any Coordinator) {
        coordinator.navController.dismiss(animated: true)
        super.didFinish(coordinator)
    }
}
