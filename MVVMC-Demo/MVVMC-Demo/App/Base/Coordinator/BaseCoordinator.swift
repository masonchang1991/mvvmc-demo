//
//  BaseCoordinator.swift
//  MVVMC-Demo
//
//  Created by Mason Chang on 2024/4/29.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    var navController: UINavigationController { get }
    var parentCoordinator: Coordinator? { get set }
    
    func start()
    func start(child coordinator: Coordinator)
    func didFinish(_ coordinator: Coordinator)
    func removeAllChildCoordinators()
}

class BaseCoordinator: Coordinator {
    var navController: UINavigationController = UINavigationController()
    var childCoordinators: [Coordinator] = []
    var parentCoordinator: Coordinator?
    
    func start() {
        fatalError("Start method should be implemented.")
    }
    
    func start(child coordinator: Coordinator) {
        childCoordinators.append(coordinator)
        coordinator.parentCoordinator = self
        coordinator.start()
    }
    
    /// Called by child coordinator
    func didFinish(_ coordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(where: { $0 === coordinator }) {
            childCoordinators.remove(at: index)
        }
    }
    
    func removeAllChildCoordinators() {
        childCoordinators.forEach { $0.removeAllChildCoordinators() }
        childCoordinators.removeAll()
    }
}
