//
//  SignInVM.swift
//  MVVMC-Demo
//
//  Created by Mason Chang on 2024/4/29.
//

import Foundation

enum SignInError: Error {
    case somethingWrong
}

protocol SignInVM {
    var delegate: SignInVMDelegate? { get set }
    func signin(name: String, password: String, completion: ((Error?) -> Void))
    func checkNameFormat(_ name: String) -> Bool
    func checkPasswordFormat(_ password: String) -> Bool
}

protocol SignInVMDelegate: AnyObject {
    func signInSuccess(_ vm: SignInVM)
    func signInFailure(_ vm: SignInVM)
}

class FakeSignInVM: SignInVM {

    private var regexChecker: RegexChecker
    
    weak var delegate: SignInVMDelegate?
    
    init(regexChecker: RegexChecker = NormalRegexChecker()) {
        self.regexChecker = regexChecker
    }
    
    func signin(name: String, password: String, completion: ((Error?) -> Void)) {
        if name == "mason" && password == "1234" {
            completion(nil)
            delegate?.signInSuccess(self)
        } else {
            completion(SignInError.somethingWrong)
            delegate?.signInFailure(self)
        }
    }
    
    func checkNameFormat(_ name: String) -> Bool {
        return regexChecker.checkNameFormat(name)
    }
    
    func checkPasswordFormat(_ password: String) -> Bool {
        return regexChecker.checkPasswordFormat(password)
    }
}
