//
//  SignInSignUpCoordinator.swift
//  MVVMC-Demo
//
//  Created by Mason Chang on 2024/4/29.
//

import Foundation

class SignInSignUpCoordinator: BaseCoordinator {
    
    private var signInViewModel: SignInVM
    private var signUpViewModel: SignUpVM
    
    init(signInViewModel: SignInVM, signUpViewModel: SignUpVM) {
        self.signInViewModel = signInViewModel
        self.signUpViewModel = signUpViewModel
    }
    
    override func start() {
        let signInVC = SignInVC()
        self.signInViewModel.delegate = self
        signInVC.viewModel = signInViewModel
        navController.pushViewController(signInVC, animated: true)
    }
    
    func goToSignUp() {
        let signUpVC = SignUpVC()
        self.signUpViewModel.delegate = self
        signUpVC.viewModel = signUpViewModel
        navController.pushViewController(signUpVC, animated: true)
    }
    
    func successSignIn() {
        parentCoordinator?.didFinish(self)
    }
}

extension SignInSignUpCoordinator: SignInVMDelegate {
    func signInSuccess(_ vm: any SignInVM) {
        successSignIn()
    }
    
    func signInFailure(_ vm: any SignInVM) {
        goToSignUp()
    }
}

extension SignInSignUpCoordinator: SignUpVMDelegate {
    func signUpSuccess(_ vm: any SignUpVM) {
        successSignIn()
    }
    
    func signUpFailure(_ vm: any SignUpVM) {
        // Do nothing
    }
}
