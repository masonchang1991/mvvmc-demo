//
//  SignInVC.swift
//  MVVMC-Demo
//
//  Created by Mason Chang on 2024/4/29.
//

import Foundation
import UIKit

class SignInVC: UIViewController {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "SignIn"
        label.font = .systemFont(ofSize: 30)
        label.textColor = .blue
        return label
    }()
    
    lazy var nameTextField: UITextField = {
        let textField = UITextField(frame: .zero)
        textField.textColor = .black
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.gray.cgColor
        return textField
    }()
    
    lazy var nameHintLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12)
        label.textColor = .gray
        return label
    }()
    
    lazy var passwordTextField: UITextField = {
        let textField = UITextField(frame: .zero)
        textField.textColor = .black
        textField.textContentType = .password
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.gray.cgColor
        return textField
    }()
    
    lazy var passwordHintLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12)
        label.textColor = .gray
        return label
    }()
    
    lazy var signInButton: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .lightGray
        btn.setTitleColor(.gray, for: .disabled)
        btn.setTitleColor(.black, for: .normal)
        btn.setTitle("SignIn", for: .normal)
        btn.isEnabled = false
        return btn
    }()
    
    private var isNameValid: Bool {
        viewModel.checkNameFormat(nameTextField.text ?? "")
    }
    
    private var isPasswordValid: Bool {
        viewModel.checkPasswordFormat(passwordTextField.text ?? "")
    }
    
    var viewModel: SignInVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bindings()
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        
        view.addSubview(titleLabel)
        view.addSubview(nameTextField)
        view.addSubview(nameHintLabel)
        view.addSubview(passwordTextField)
        view.addSubview(passwordHintLabel)
        view.addSubview(signInButton)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 200),
            titleLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 100),
            titleLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -100),
            titleLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor)
        ])
        
        nameTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nameTextField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 50),
            nameTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 100),
            nameTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -100),
            nameTextField.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor)
        ])
        
        nameHintLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nameHintLabel.topAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: 5),
            nameHintLabel.leadingAnchor.constraint(equalTo: nameTextField.leadingAnchor, constant: 5),
            nameHintLabel.trailingAnchor.constraint(equalTo: nameTextField.trailingAnchor, constant: -5),
            nameTextField.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor)
        ])
        
        
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            passwordTextField.topAnchor.constraint(equalTo: nameHintLabel.bottomAnchor, constant: 20),
            passwordTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 100),
            passwordTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -100),
            passwordTextField.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor)
        ])
        
        passwordHintLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            passwordHintLabel.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 5),
            passwordHintLabel.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor, constant: 5),
            passwordHintLabel.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor, constant: -5),
            passwordHintLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor)
        ])
        
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            signInButton.topAnchor.constraint(equalTo: passwordHintLabel.bottomAnchor, constant: 50),
            signInButton.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor, constant: 5),
            signInButton.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor, constant: -5),
            signInButton.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor)
        ])
    }
    
    private func bindings() {
        nameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        signInButton.addTarget(self, action: #selector(tapSignIn), for: .touchUpInside)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == nameTextField {
            if isNameValid {
                nameHintLabel.text = ""
            } else {
                nameHintLabel.text = "xxxx"
                return
            }
        }
        
        if textField == passwordTextField {
            if isPasswordValid {
                passwordHintLabel.text = ""
            } else {
                passwordHintLabel.text = "xxxx"
                return
            }
        }
        
        signInButton.isEnabled = isNameValid && isPasswordValid
    }
    
    @objc func tapSignIn() {
        viewModel.signin(name: nameTextField.text ?? "",
                         password: passwordTextField.text ?? "") { error in
            print("\(error)")
        }
    }
}
