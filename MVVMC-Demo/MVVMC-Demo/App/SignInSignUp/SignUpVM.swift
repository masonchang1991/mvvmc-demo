//
//  SignUpVM.swift
//  MVVMC-Demo
//
//  Created by Mason Chang on 2024/4/29.
//

import Foundation

protocol SignUpVM {
    var delegate: SignUpVMDelegate? { get set }
    func signup(name: String, password: String, completion: ((Error?) -> Void))
    func checkNameFormat(_ name: String) -> Bool
    func checkPasswordFormat(_ password: String) -> Bool
}

enum SignUpError: Error {
    case unknown
}

protocol SignUpVMDelegate: AnyObject {
    func signUpSuccess(_ vm: SignUpVM)
    func signUpFailure(_ vm: SignUpVM)
}

class FakeSignUpVM: SignUpVM {
    
    private var regexChecker: RegexChecker
    
    weak var delegate: SignUpVMDelegate?
    
    init(regexChecker: RegexChecker = NormalRegexChecker()) {
        self.regexChecker = regexChecker
    }
    
    func signup(name: String, password: String, completion: ((Error?) -> Void)) {
        if checkNameFormat(name) && checkPasswordFormat(password) {
            completion(nil)
            delegate?.signUpSuccess(self)
        } else {
            completion(SignUpError.unknown)
            delegate?.signUpFailure(self)
        }
    }
    
    func checkNameFormat(_ name: String) -> Bool {
        return regexChecker.checkNameFormat(name)
    }
    
    func checkPasswordFormat(_ password: String) -> Bool {
        return regexChecker.checkPasswordFormat(password)
    }
}

