//
//  Rules.swift
//  MVVMC-Demo
//
//  Created by Mason Chang on 2024/4/29.
//

import Foundation

enum Rules:String, CaseIterable {
    case alphaRule = "[^a-zA-Z0-9-]"
}

protocol RegexChecker {
    func checkNameFormat(_ name: String) -> Bool
    func checkPasswordFormat(_ password: String) -> Bool
}

struct NormalRegexChecker: RegexChecker {
    func checkNameFormat(_ name: String) -> Bool {
        for rule in Rules.allCases {
            if name.range(of: rule.rawValue, options: .regularExpression) != nil {
                return false
            }
        }
        return true
    }
    
    func checkPasswordFormat(_ password: String) -> Bool {
        return checkNameFormat(password)
    }
}
